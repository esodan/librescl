/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* librescl
 *
 * Copyright (C) 2013. 2014 Daniel Espinosa <esodan@gmail.com>
 *
 * librescl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librescl is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GXml;
using Lscl;
using GLib;

public class Performance
{
  public static int main (string[] args)
  {
		GLib.Test.init (ref args);
    Test.add_func ("/librescl/performance/read",
    () => {
      try {
        var d = new SclDocument ();
        var f = GLib.File.new_for_path (LsclTest.TEST_DIR + "/tests-files/generic.cid");
        assert (f.query_exists ());
        d.read_from_file (f);
      }
      catch (GLib.Error e) {
        Test.message (e.message);
        assert_not_reached ();
      }
    });
		return GLib.Test.run ();
  }
}
