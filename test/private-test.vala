/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* librescl
 *
 * Copyright (C) 2013. 2014 Daniel Espinosa <esodan@gmail.com>
 *
 * librescl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * librescl is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Lscl;
using GXml;

public static int main (string[] args)
{
	GLib.Test.init (ref args);
  Test.add_func ("/librescl/private/enable",
  () => {
    try {
      string path = LsclTest.TEST_DIR + "/tests-files/scl-private.cid";
      var f = File.new_for_path (path);
      assert (f.query_exists ());
      var d = new SclDocument ();
      d.read_from_file (f);
      assert (d.scl.privates != null);
      assert (d.scl.privates.length == 1);
      var p = d.scl.privates.get_item (0) as tPrivate;
      assert (p != null);
      assert (p.private_type == "lscl");
      assert (p.source == "http://www.librescl.org");
    }
    catch (GLib.Error e)
    {
      GLib.message (@"ERROR: $(e.message)");
      assert_not_reached ();
    }
  });
	return GLib.Test.run ();
}
