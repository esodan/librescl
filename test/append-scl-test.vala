/**
 *
 *  LibreSCL
 *
 *  Authors:
 *
 *       Daniel Espinosa <esodan@gmail.com>
 *
 *
 *  Copyright (c) 2024 Daniel Espinosa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
     *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 using Lscl;
 using GXml;
 
 
 public static int main (string[] args)
 {
    GLib.Test.init (ref args);
    Test.add_func ("/librescl/append-scl",
    () => {
        var loop = new MainLoop ();
        Idle.add (()=>{
            message ("Initializing Test");
            int ieds = 0;
            int subnetworks = 0;
            int lnts = 0;
            int dots = 0;
            int dats = 0;
            int enumts = 0;
            string path = LsclTest.TEST_DIR + "/tests-files/ied-complete.scd";
            var f = File.new_for_path (path);
            assert (f.query_exists ());
            var d = new SclDocument ();
            var d2 = new SclDocument ();
            try {
                d.read_from_file (f);
                string output = d.write_string ();
                assert (d.scl.ieds.length == 1);
                foreach (DomElement e in d.scl.children) {
                    if (e is tIED) {
                        ieds++;
                    }
                    if (e is tCommunication) {
                        foreach (DomElement ec in e.children) {
                            if (ec is tSubNetwork) {
                                subnetworks++;
                            }
                        }
                    }
                    if (e is tDataTypeTemplates) {
                        foreach (DomElement et in e.children) {
                            if (et is tLNodeType) {
                                lnts++;
                            }
                            if (et is tDOType) {
                                dots++;
                            }
                            if (et is tDAType) {
                                dats++;
                            }
                            if (et is tEnumType) {
                                enumts++;
                            }
                        }
                    }
                }
                message ("Estatistics: IEDs: %d, SubNets: %d, LNT: %d, DOT: %d, DAT: %d, ENUMT: %d",
                        ieds, subnetworks, lnts, dots, dats, enumts);
                d2.read_from_string (output);
            } catch (GLib.Error e) {
                warning ("ERROR: %s", e.message);
            }
            // append the new document
            d.append.begin (d2, (obj, res)=>{
                try {
                    d.append.end (res);
                    assert (d.scl.ieds.length == 2);

                    ieds = 0;
                    subnetworks = 0;
                    lnts = 0;
                    dots = 0;
                    dats = 0;
                    enumts = 0;
                    foreach (DomElement e in d.scl.children) {
                        if (e is tIED) {
                            ieds++;
                        }
                        if (e is tCommunication) {
                            foreach (DomElement ec in e.children) {
                                if (ec is tSubNetwork) {
                                    subnetworks++;
                                }
                            }
                        }
                        if (e is tDataTypeTemplates) {
                            foreach (DomElement et in e.children) {
                                if (et is tLNodeType) {
                                    lnts++;
                                }
                                if (et is tDOType) {
                                    dots++;
                                }
                                if (et is tDAType) {
                                    dats++;
                                }
                                if (et is tEnumType) {
                                    enumts++;
                                }
                            }
                        }
                    }
                } catch (GLib.Error e) {
                    warning ("Error: %s", e.message);
                }
                message ("Estatistics: IEDs: %d, SubNets: %d, LNT: %d, DOT: %d, DAT: %d, ENUMT: %d",
                        ieds, subnetworks, lnts, dots, dats, enumts);
                loop.quit ();
            });
            return Source.REMOVE;
        });
        loop.run ();
    });
   return GLib.Test.run ();
 }
 