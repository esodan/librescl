/*
 *
 *  LibreSCL
 *
 *  Authors:
 *
 *       Daniel Espinosa <esodan@gmail.com>
 *       PowerMedia Consulting <pwmediaconsulting@gmail.com>
 *
 *
 *  Copyright (c) 2013-2024 Daniel Espinosa
 *  Copyright (c) 2014 PowerMedia Consulting
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GXml;
using Xml;

public errordomain Lscl.SclDocumentError {
  STRUCTURE_ERROR
}

/**
 * An implementation of {@link GXml.Document} to handle
 * SCL documents.
 */
public class Lscl.SclDocument : GXml.Document {
    [Description (nick="::root")]
    public Scl scl { get; set; }

    /**
    * Add toplevel SCL root XML element
    */
    public void add_scl () throws GLib.Error {
        if (document_element != null) {
            throw new SclDocumentError.STRUCTURE_ERROR ("Document already have a root element");
        }
        scl = GLib.Object.new (typeof (Scl), "owner-document", this) as Scl;
        try {
            append_child (scl);
        } catch (GLib.Error e) {
            warning ("Append SCL root element error: %s", e.message);
        }
    }
    /**
    * A on-the-fly-post-parsing implemention helper method
    * to be used on large SCL document.
    *
    * The technique reads the document faster that standard
    * implementation and is the user responsability to call
    * {@link GXml.Element.parse_buffer} on {@link scl} in order
    * to get a fully parsed document or call over any child
    * element to a quick access to parts in the document.
    *
    * @param stream an {@link GLib.InputStream} to read data from
    */
    public void read (InputStream stream) throws GLib.Error {
        read_from_stream (stream);
    }
    /**
    * Asychronically parse on-the-fly-post-parsing
    * an SCL document see {@link read} for details.
    */
    public async void read_async (InputStream stream) throws GLib.Error {
        read (stream);
    }
    /**
    * Read and append all IED, Data Type Templates
    * and networks in the given document.
    *
    * No history is added.
    */
    public async bool append (SclDocument doc) throws GLib.Error
    {
        foreach (DomElement e in doc.scl.children) {
            if (e is tIED) {
                tIED ied = scl.create_ied ();
                string str = e.write_string ();
                ied.read_from_string (str);
                scl.ieds.append (ied);
            }
        }
        foreach (DomElement e in scl.data_type_templates.children) {
            if (e is tLNodeType) {
                tLNodeType lnt = scl.data_type_templates.create_logical_node_type ();
                string str = e.write_string ();
                lnt.read_from_string (str);
                scl.data_type_templates.logical_node_types.append (lnt);
            }
            if (e is tDOType) {
                tDOType dot = scl.data_type_templates.create_data_object_type ();
                string str = e.write_string ();
                dot.read_from_string (str);
                scl.data_type_templates.data_object_types.append (dot);
            }
            if (e is tDAType) {
                tDAType dat = scl.data_type_templates.create_data_attribute_type ();
                string str = e.write_string ();
                dat.read_from_string (str);
                scl.data_type_templates.data_attribute_types.append (dat);
            }
            if (e is tEnumType) {
                tEnumType enumt = scl.data_type_templates.create_enum_type ();
                string str = e.write_string ();
                enumt.read_from_string (str);
                scl.data_type_templates.enum_types.append (enumt);
            }
            if (e is tSubstation) {
                tSubstation se = scl.create_substation ();
                string str = e.write_string ();
                se.read_from_string (str);
                scl.substations.append (se);
            }
        }
        foreach (DomElement e in scl.communication.children) {
            if (e is tSubNetwork) {
                tSubNetwork sn = scl.communication.create_subnetwork ();
                string str = e.write_string ();
                sn.read_from_string (str);
                scl.communication.subnetworks.append (sn);
            }
        }
        return true;
    }
}
