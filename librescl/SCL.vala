/*
 *  LibreSCL
 *
 *  Authors:
 *
 *       Daniel Espinosa <esodan@gmail.com>
 *       PowerMedia Consulting <pwmediaconsulting@gmail.com>
 *
 *
 *  Copyright (c) 2013-2024 Daniel Espinosa
 *  Copyright (c) 2014 PowerMedia Consulting
 *  
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 3 of the License, or (at your option) any later version.
 *  
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

public class Lscl.Scl : tBaseElement
{
    public tHeader header { get; set; }
    public tCommunication communication { get; set; }
    public tIED.HashMap ieds { get; set; }
    public tDataTypeTemplates data_type_templates { get; set; }
    public tSubstation.ArrayList substations { get; set; }

    construct {
      try {
        initialize_with_namespace ("http://www.iec.ch/61850/2003/SCL", "scl", "SCL");
        set_attribute_ns ("http://www.w3.org/2000/xmlns/","xmlns:lscl","http://www.librescl.org/SCL");
        set_attribute_ns ("http://www.w3.org/2000/xmlns/","xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
        set_instance_property ("ieds");
        set_instance_property ("substations");
      } catch (GLib.Error e)  {
        warning ("Error: "+e.message);
      }
    }

    public new string to_string ()
    {
        string str = "SCL File:\n";
        if (header != null)
            str += "Version: " + header.version + "\n"
            + "Revision: " + header.revision + "\n";
        if (communication != null) {
            if (communication.subnetworks != null)
                str += "Subnetworks: "+ communication.subnetworks.length.to_string () + "\n";
        }
        if (ieds != null)
            str += "Total IED number: " + ieds.length.to_string () + "\n";
        if (data_type_templates != null) {
            if (data_type_templates.logical_node_types != null)
                str += "Total LN Types: " + data_type_templates.logical_node_types.length.to_string ()
                + "\n";
            if (data_type_templates.data_object_types != null)
                str += "Total DO Types: " + data_type_templates.data_object_types.length.to_string ()
                + "\n";
            if (data_type_templates.data_attribute_types != null)
                str += "Total DA Types: " + data_type_templates.data_attribute_types.length.to_string ()
                + "\n";
            if (data_type_templates.enum_types != null)
                str += "Total Enum Types: " + data_type_templates.enum_types.length.to_string ()
                + "\n";
        }
        return str;
    }
    /**
    * Create the header
    */
    public void create_header () {
      set_instance_property ("header");
    }
    /**
    * Create communication
    */
    public void create_communication () {
      set_instance_property ("communication");
    }
    /**
    *
    */
    public void create_data_type_templates () {
      set_instance_property ("data_type_templates");
    }
    /**
     * Create an IED.
     *
     * You should use {@link GXml.Collection.append} on
     * {@link ieds} in order to make
     * it part of the configuration
     */
    public tIED create_ied () {
        return (tIED) ieds.create_item ();
    }
    /**
     * Create a Substation, defined as a voltage level
     * in a substation with multiple voltage levels, so
     * this could be more than just {@link tSubstation}
     * defined.
     *
     * You should use {@link GXml.Collection.append} on
     * {@link substations} in order to make
     * it part of the configuration
     */
    public tSubstation create_substation () {
        return (tSubstation) substations.create_item ();
    }
}

